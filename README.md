# RCake
### _A code maker written in Rust, because why not._

## Features
- [x] Basic commands
- [x] Recipes
- [x] Directory context
- [x] Constants

## Bootstrapping

So you can actually bootstrap RCake by first compiling it with rustc (or cargo) and then recompiling it with the RCake file.

```bash
$ mkdir -p dst
$ cd src
$ rustc -o ../dst/rcake rcake.rs
$ ../dst/rcake rcake 
$ ../dst/rcake rcake 
(...)
$ ../dst/rcake rcake 
```

## Usage

See the [docs](docs/) directory for more information about this project.
