mod logger;
mod interpreter;

use std::env;
use std::io::Read;
use std::fs::File;
use std::path::Path;
use logger::logger::log_error;
use interpreter::interpreter::interpret_rcake_src;

pub fn read_rcake_src(file: &mut File, mut args: Vec<String>) {
    let mut src = String::new();
    match file.read_to_string(&mut src) {
	Err(_) => log_error("stdfs", "could not read RCake file."),
	Ok(_) => interpret_rcake_src(&mut src, &mut args),
    }
}

fn main() {
    let path = Path::new("RCake");
    let args: Vec<String> = env::args().collect();
    let mut file: File;

    match File::open(&path) {
	Err(_) => log_error("stdfs", "could not open RCake file."),
	Ok(f) => { file = f; read_rcake_src(&mut file, args) },
    };
}
