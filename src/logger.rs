pub mod logger {
    use std::process;

    pub fn log_error(typ: &str, msg: &str) {
	println!("[RCAKE] {}: error: {}", typ, msg);
	process::exit(1);
    }

    pub fn log_msg(msg: &str) {
	println!("[RCAKE] {}", msg);
    }

    pub fn log_cmd(cmd: &str) {
	println!("[CMD] {}", cmd);
    }
}
