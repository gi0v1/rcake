pub mod interpreter {
    use logger::logger::*;
    use std::process::Command;
    use std::collections::HashMap;

    #[derive(Clone)]
    pub struct Recipe {
	recipe_name: String,
	recipe_contents: Vec<String>,
	end: usize,
    }
    
    #[derive(Clone)]
    pub struct RCake {
	recipes: Vec<Recipe>,
	consts: HashMap<String, String>,
	root: String,
    }

    pub fn interpret_rcake_src(src: &mut String, args: &mut Vec<String>) {
	let src_arr = src.split("\n").collect::<Vec<&str>>();

	let mut rcake = RCake {
	    recipes: Vec::new(),
	    consts: HashMap::new(),
	    root: String::from(".")
	};

	let mut last_recipe: Recipe = parse_source(&mut rcake, &mut src_arr.clone(), 0);

	while last_recipe.end != 0 {
	    rcake.recipes.push(last_recipe.clone());
	    last_recipe = parse_source(&mut rcake, &mut src_arr.clone(), last_recipe.end);
	}

	log_msg(format!("Entering directory '{}'", rcake.root).as_str());

	args.remove(0);

	rcake = apply_consts(rcake.clone());

	for arg in args {
	    execute_recipe_by_name(&rcake, &rcake.recipes, arg.to_string());
	}

	log_msg(format!("Leaving directory '{}'", rcake.root).as_str());
    }

    pub fn apply_consts(rcake: RCake) -> RCake {
	let mut rcake_ret = rcake.clone();

	rcake_ret.root = str_apply_consts(rcake.clone(), rcake.root.clone());

	for i in 0..rcake.recipes.len() {
	    rcake_ret.recipes[i].recipe_name = str_apply_consts(rcake.clone(),
								rcake.recipes[i].recipe_name.clone());

	    for j in 0..rcake.recipes[i].recipe_contents.len() {
		rcake_ret.recipes[i].recipe_contents[j] = str_apply_consts(rcake.clone(),
									   rcake.recipes[i].recipe_contents[j].clone());
	    }
	}

	return rcake_ret;
    }

    pub fn str_apply_consts(rcake: RCake, word: String) -> String {
	let mut const_name: String = String::from("");
	let mut word_ret_arr: String = String::from("");
	let mut is_const = false;

	for c in word.chars() {
	    if is_const {
		if c == ')' {
		    for (cn, cc) in &rcake.consts {
			if const_name.eq(cn) {
			    word_ret_arr.push_str(cc);
			}
		    }

		    is_const = false;
		    continue;
		}

		if c == '(' {
		    continue;
		}

		const_name.push(c);
		continue;
	    }

	    if c == '$' {
		is_const = true;
		const_name = String::from("");
	    } else {
		word_ret_arr.push(c);
	    }
	}

	return word_ret_arr;
    }

    pub fn execute_recipe_by_name(rcake: &RCake, recipes: &Vec<Recipe>, name: String) {
	for recipe in recipes.clone() {
	    if recipe.recipe_name.eq(name.as_str()) {
		execute_recipe(&rcake, &recipe);
		return;
	    }
	}

	log_error("rcake", format!("recipe `{}` not found in RCake file.", name).as_str())
    }

    pub fn execute_recipe(rcake: &RCake, recipe: &Recipe) {
	log_msg(format!("Baking recipe `{}`", recipe.recipe_name).as_str());

	let mut current_dir: String = String::from(".");

	for cmd in recipe.clone().recipe_contents {

	    let mut full_cmd = Vec::new();
	    let mut argv = cmd.split(" ").collect::<Vec<&str>>();
	    let mut j: Vec<usize> = Vec::new();
	    let mut s: usize = 0;

	    for i in 0..argv.len() {
		if argv[i].chars().count() <= 0 {
		    j.push(i);
		}
	    }

	    for k in j {
		argv.remove(k - s);
		s += 1;
	    }

	    if argv[0].eq("directory") {
		current_dir = argv[1].to_string();
		continue;
	    }

	    log_msg(format!("Entering directory '{}'", current_dir).as_str());

	    full_cmd.push("cd");
	    full_cmd.push(rcake.root.as_str());
	    full_cmd.push("&&");
	    full_cmd.push("cd");
	    full_cmd.push(current_dir.as_str());
	    full_cmd.push("&&");

	    log_cmd(argv.join(" ").as_str());

	    full_cmd.append(&mut argv);

	    let output = Command::new("sh")
		.arg("-c")
		.arg(full_cmd.join(" ").as_str())
		.output()
		.expect("failed to execute process");

	    print!("{}", String::from_utf8_lossy(&output.stdout));
	    print!("{}", String::from_utf8_lossy(&output.stderr));

	    if !output.status.success() {
		log_error("sysexec", "process exited with non-zero status.");
	    }

	    log_msg(format!("Leaving directory '{}'", current_dir).as_str());
	}
    }

    pub fn parse_source(rcake: &mut RCake, src_arr: &mut Vec<&str>, start: usize) -> Recipe {
	let src_arr_clone = src_arr.clone();
	let len = src_arr_clone.len();

	if start >= len - 1 {
	    return Recipe {
		recipe_name: "end".to_string(),
		recipe_contents: Vec::new(),
		end: 0, // <--- End identifier
	    };
	}

	for i in start..len {
	    let line = src_arr_clone[i];
	    let line_split = line.split(" ").collect::<Vec<&str>>();

	    if line_split[0].eq("recipe") {
		let mut recipe = Recipe {
		    recipe_name: line_split[1].to_string(),
		    recipe_contents: Vec::new(),
		    end: 0,
		};

		parse_recipe_until(&mut recipe, src_arr, "end".to_string(), i + 1, len);

		return recipe;
	    } else if line_split[0].eq("root") {
		rcake.root = line_split[1].to_string();
	    } else if line_split[0].eq("const") {
		rcake.consts.insert(line_split[1].to_string(), line_split[2..].join(" "));
	    }
	}

	log_error("rcake", "there are no recipes in the file.");
	return Recipe { end: 0, recipe_name: "".to_string(), recipe_contents: Vec::new() };
    }

    pub fn parse_recipe_until(recipe: &mut Recipe, src_arr: &mut Vec<&str>,
			      word: String, start: usize, len: usize) {
	for i in start..len {
	    let line = src_arr.clone()[i];

	    if line.eq(word.as_str()) { recipe.end = i + 1; break; }

	    recipe.recipe_contents.push(line.to_string());
	}
    }
}
