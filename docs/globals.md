# Globals

## Constants

Using the `const` directive you can define a constant with this syntax:

```
const MY_CONST my constant value (...)
```

Then they can be referenced using `$(MY_CONST)`.

NOTE: The constant ends on newlines, for now there are no escapes (\)

## Root

Using the `root` directive you can define the root for all the recipes, you can then use the `directory` directive to go into subdirectories of `root`, for that see [recipes](recipes.md).

NOTE: If the root directory is not defined it will default to the current directory (.)
