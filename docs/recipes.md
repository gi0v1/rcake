# Recipes

You can define a recipe like so:

```
recipe my_recipe
	(commands)
end
```

## Directory

Using the `directory` directive you can go from one directory to another based on the `root`.

```
recipe my_recipe
	directory my_directory
	echo $PWD # Output = root/my_directory
	directory my_directory_2
	echo $PWD # Output = root/my_directory_2
end
```

NOTE: If no directory is defined in a recipe it will default to the root definition, if that is not defined, it will default to the current directory.
