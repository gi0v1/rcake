(defconst rcake-mode-syntax-table
  (with-syntax-table (copy-syntax-table)
    (modify-syntax-entry ?' "\"")
    (syntax-table))
  "Syntax table for `rcake-mode'.")

(eval-and-compile
  (defconst rcake-keywords
    '("recipe" "end" "directory" "root" "const")))

(defconst rcake-highlights
  `((,(regexp-opt rcake-keywords 'symbols) . font-lock-keyword-face)))

;;;###autoload
(define-derived-mode rcake-mode prog-mode "rcake"
  "Major Mode for editing RCake."
  :syntax-table rcake-mode-syntax-table
  (setq font-lock-defaults '(rcake-highlights))
  (setq-local comment-start "#"))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\RCake\\'" . rcake-mode))

(provide 'rcake-mode)
